// Obtenemos el button y lo almacenamos en una variable llamada "btn"
var btn = document.getElementById("btn");
var btn2 = document.getElementById("btn2");
var btn3 = document.getElementById("btn3");
var btn4 = document.getElementById("btn4");
/* Obtenemos el div que muestra el resultado y lo 
almacenamos en una variable llamada "resultado" */ 
var resultado = document.getElementById("resultado")
/* Obtenemos los dos input y los almacenamos en 
variables "inputUno" y "inputDos" */
var inputUno = document.getElementById("input-uno");
var inputDos = document.getElementById("input-dos");
// Añadimos el evento click a la variable "btn"
btn.addEventListener("click",function(){
    /* Obtenemos el valor de cada input accediendo a 
    su atributo "value" */
    var n1 = inputUno.value;
    var n2 = inputDos.value;
    /* Llamamos a una función que permite realizar la 
    suma de los números y los mostramos al usuario" */ 

    if(is_entero(n1) && is_entero(n2)){

        var result = suma(n1,n2)

        if(isNaN(result)){
            resultado.innerHTML = "DATO FALTANTE";
            resultado.style.color ="red";
        }else{
            resultado.innerHTML = result;
            resultado.style.color ="black";
        }

    }else{
        resultado.innerHTML = "NUMERO NO ES ENTERO";
        resultado.style.color ="red";
    }
    
    
    
    
});

btn2.addEventListener("click",function(){
    var n1 = inputUno.value;
    var n2 = inputDos.value;

    if(is_entero(n1) && is_entero(n2)){
        
        var result = resta(n1,n2)

        if(isNaN(result)){
            resultado.innerHTML = "DATO FALTANTE";
            resultado.style.color ="red";
        }else{
            resultado.innerHTML = result;
            resultado.style.color ="black";
        }

    }else{
        resultado.innerHTML = "NUMERO NO ES ENTERO";
        resultado.style.color ="red";
    }

    

});

btn3.addEventListener("click",function(){
    var n1 = inputUno.value;
    var n2 = inputDos.value;

    if(is_entero(n1) && is_entero(n2)){
        
        var result = multiplicar(n1,n2)

        if(isNaN(result)){
            resultado.innerHTML = "DATO FALTANTE";
            resultado.style.color ="red";
        }else{
            resultado.innerHTML = result;
            resultado.style.color ="black";
    }
    }else{
        resultado.innerHTML = "NUMERO NO ES ENTERO";
        resultado.style.color ="red";
    }

    


});

btn4.addEventListener("click", function(){
    var n1 = inputUno.value;
    var n2 = inputDos.value;

    if(is_entero(n1) && is_entero(n2)){
        
        var result = dividir(n1, n2)

        
        if(parseInt(n2) === 0){
            resultado.innerHTML = "ERROR";
            resultado.style.color ="red";
        }else{
            if(isNaN(result)){
                    resultado.innerHTML = "DATO FALTANTE";
                    resultado.style.color ="red";
            }else{
                    resultado.innerHTML = result;
                    resultado.style.color ="black";
             }
        } 
    }else{
        resultado.innerHTML = "NUMERO NO ES ENTERO";
        resultado.style.color ="red";
    }



});

/* Función que retorna la suma de dos números */
function suma(n1, n2){
    // Es necesario aplicar parseInt a cada número
    return parseInt(n1) + parseInt(n2);
}

function resta(n1, n2){
    // Es necesario aplicar parseInt a cada número
    return parseInt(n1) - parseInt(n2);
}

function multiplicar(n1, n2){
    // Es necesario aplicar parseInt a cada número
    return parseInt(n1) * parseInt(n2);
}

function dividir(n1, n2){
    // Es necesario aplicar parseInt a cada número
    return parseInt(n1) / parseInt(n2); 
}
    
function is_entero(val){
    if (val%1 == 0){
        return true;
    }else{
        return false;
    }
}
    
    

